function loadDoc() {
      let xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

                var response = JSON.parse(this.responseText);
                let table = document.querySelector("table");
                let data = Object.keys(response[0]);
                generateTableHead(table, data);
                generateTable(table, response);
            }
      };
      xhttp.open("POST", "/RequestEmployeeData", true);
      xhttp.send();
}
function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}

function generateTable(table, data) {
  for (let element of data) {
    let row = table.insertRow();
    for (key in element) {
          let cell = row.insertCell();
          let text = document.createTextNode(element[key]);
          if (element.status === 'p'){
               row.setAttribute("name","P");
          }
          else if (element.status === 'a'){
               row.setAttribute("name","A");
          }
          else if (element.status === 'd'){
               row.setAttribute("name","D");
          }
      cell.appendChild(text);
    }
    //add button for manager approval
  }
}

function Pending(){
    let p = document.getElementsByName("P");
    for (var index = 0;index<p.length;index++){
        p[index].style.display = "table-row";
    }
    let a = document.getElementsByName("A");
    for (var index = 0;index<a.length;index++){
        a[index].style.display = "none";
    }
    let d = document.getElementsByName("D");
    for (var index = 0;index<d.length;index++){
        d[index].style.display = "none";
    }
}

function Approved(){
    let p = document.getElementsByName("P");
    for (var index = 0;index<p.length;index++){
        p[index].style.display = "none";
    }
    let a = document.getElementsByName("A");
    for (var index = 0;index<a.length;index++){
        a[index].style.display = "table-row";
    }
    let d = document.getElementsByName("D");
    for (var index = 0;index<d.length;index++){
        d[index].style.display = "none";
    }
}

function Denied(){
    let p = document.getElementsByName("P");
    for (var index = 0;index<p.length;index++){
        p[index].style.display = "none";
    }
    let a = document.getElementsByName("A");
    for (var index = 0;index<a.length;index++){
        a[index].style.display = "none";
    }
    let d = document.getElementsByName("D");
    for (var index = 0;index<d.length;index++){
        d[index].style.display = "table-row";
    }
}

function All(){
    let p = document.getElementsByName("P");
    for (var index = 0;index<p.length;index++){
        p[index].style.display = "table-row";
    }
    let a = document.getElementsByName("A");
    for (var index = 0;index<a.length;index++){
        a[index].style.display = "table-row";
    }
    let d = document.getElementsByName("D");
    for (var index = 0;index<d.length;index++){
        d[index].style.display = "table-row";
    }
}

loadDoc();

Pending();