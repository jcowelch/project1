package Utility;
import java.util.HashMap;

/**
 * Class for an an abstract application method. Acts as a general use blueprint for an application
 *
 * @author Jeremy Cowelchuk
 */
public abstract class AbstractApplication {
    /**
     * Runs the application. Generally one instance of this call in Main() method
     */
    public abstract void run();

    /**
     * Quits the application and does closing actions
     */
    public abstract void quit();

    /**
     * Initializes the application such that runtime is as intented.
     */
    protected abstract void initialize(String dbName);
}