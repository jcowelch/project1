package Utility;
import org.apache.logging.log4j.LogManager;

/**
 * Interface for logging the application.
 *
 * @author Jeremy Cowelchuk
 */
public interface Logging {
    org.apache.logging.log4j.Logger rootLogger = LogManager.getRootLogger();

    /**
     * Logs the data
     */
    void logData();
}
