package Controllers;

import POJOS.Employee;
import Service.EmployeeService;
import Utility.Logging;
import com.mongodb.client.MongoCollection;
import io.javalin.http.Context;
import org.json.JSONArray;

/**
 * Controller for handling all employee related actions.
 *
 * @author Jeremy
 */
public class EmployeeController implements Controller, Logging {
    EmployeeService service;
    private String currentUser = null;

    /**
     * Constructor for the employee controller. Takes a log in service
     * for handling services related to the employee.
     * @param logInService The service put in
     */
    public EmployeeController(EmployeeService logInService)
    {
        service = logInService;
    }

    /**
     * Logs in an employee
     * @param ctx The inputted context
     */
    public void LogInEmployee(Context ctx){
        String username = ctx.formParam("username");
        String password = ctx.formParam("password");
        if (service.employeeLogIn(username, password)) {
            ctx.render("/public/EmployeeHome.html");
            currentUser = username;
            logData();
        }
        else {
            // do something here to say that it was a failed attempt
            ctx.render("/public/EmployeeLogIn.html");
        }
    }

    /**
     * Prepares an employees data on the page
     * @param ctx The inputted context
     */
    public void GetEmployeeInformation(Context ctx) {
        Employee currentEmployee = service.getDao().getEmployee(currentUser);
        ctx.json(currentEmployee);
    }

    /**
     * Gets the current user
     * @return The current user
     */
    public String getCurrentUser(){
        return currentUser;
    }

    /**
     * Changes the current employee password.
     * @param ctx The inputted context
     */
    public void ChangePassword(Context ctx) {
        String newPassword = ctx.formParam("updatePassword");
        service.getDao().UpdateEmployeePassword(currentUser,newPassword);
        logDataPassword();
        ctx.redirect("/EmployeeHome");
    }

    /**
     * Checks the LogIn status of the Employee
     */
    public boolean CheckLogInStatus(){
        if (currentUser == null){
            return false;
        }
        return true;
    }

    /**
     * Logs out the current user
     */
    public void logOut() {
        currentUser = null;
    }

    /**
     * Gets all the employees usernames and displays them
     * @param ctx The context of the screen
     */
    public void getEmployees(Context ctx) {
        MongoCollection<Employee> employees = service.getEmployees();
        JSONArray response = new JSONArray();
        for (Employee employee : employees.find()) {
            response.put(employee);
        }
        ctx.json(response);
    }

    /**
     * Logs the information on log ins
     */
    @Override
    public void logData() {
        rootLogger.info("Employee {} has successfully logged in.",currentUser);
    }

    /**
     * Logs the information for user passwords
     */
    private void logDataPassword() {
        rootLogger.info("Employee {} has changed password",currentUser);
    }
}
