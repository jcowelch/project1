package Controllers;

import io.javalin.http.Context;


/**
 * Displays all the information for the user. Also routs the user if their logged in or not
 * @author Jeremy
 */
public class DisplayController implements Controller{
    private ManagerController managerController;
    private EmployeeController employeeController;

    /**
     * Constructor for the display class.
     * @param employeeController The employee controller
     * @param managerController The manager controller
     */
    public DisplayController(EmployeeController employeeController, ManagerController managerController){
        this.employeeController = employeeController;
        this.managerController = managerController;
    }

    /**
     * Check display for the employee home
     * @param ctx the context of the screen
     */
    public void displayEmployeeHome(Context ctx){
        if (employeeController.CheckLogInStatus()){
            ctx.render("/public/EmployeeHome.html");
        }
        else {
            ctx.redirect("/Employee");
        }
    }

    /**
     * Check display for submitting requests
     * @param ctx the context of the screen
     */
    public void displaySubmitRequest(Context ctx) {
        if (employeeController.CheckLogInStatus()){
            ctx.render("/public/SubmitRequest.html");
        }
        else {
            ctx.redirect("/Employee");
        }
    }

    /**
     * Check display for viewing employee requests
     * @param ctx the context of the screen
     */
    public void displayViewRequests(Context ctx) {
        if (employeeController.CheckLogInStatus()){
            ctx.render("/public/ViewRequests.html");
        }
        else {
            ctx.redirect("/Employee");
        }
    }

    /**
     * Check display for updating an employee
     * @param ctx the context of the screen
     */
    public void displayUpdate(Context ctx) {
        if (employeeController.CheckLogInStatus()){
            ctx.render("/public/Update.html");
        }
        else {
            ctx.redirect("/Employee");
        }
    }

    /**
     * Check display for the manager home screen
     * @param ctx the context of the screen
     */
    public void displayManagerHome(Context ctx) {
     if (managerController.CheckLogInStatus()){
         ctx.render("/public/ManagerHome.html");
     }
     else {
         ctx.redirect("/Manager");
     }
    }

    /**
     * Displays manager requests and redirects if wrong
     * @param ctx The context of the screen
     */
    public void displayManagerRequests(Context ctx) {
        if (managerController.CheckLogInStatus()){
            ctx.render("/public/ManagerRequests.html");
        }
        else {
            ctx.redirect("/Manager");
        }
    }

    /**
     * Displays the home screen and logs out both users
     * @param ctx The context of the screen
     */
    public void displayHomeScreen(Context ctx) {
        managerController.logOut();
        employeeController.logOut();
        ctx.render("public/index.html");
    }

    /**
     * Display the manager view screen and confirms log in
     * @param ctx The context of the screen
     */
    public void displayManagerView(Context ctx) {
        if (managerController.CheckLogInStatus()){
            ctx.render("/public/ManagerView.html");
        }
        else {
            ctx.redirect("/Manager");
        }
    }
}
