package Controllers;

import POJOS.Employee;
import POJOS.Manager;
import Service.ManagerService;
import Utility.Logging;
import io.javalin.http.Context;

/**
 * Class for controlling all manager related actions
 * @author Jeremy
 */
public class ManagerController implements Logging {
    private ManagerService managerService;
    private String currentUser = null;

    /**
     * Constructor for the manager controller
     * @param managerService The manager service
     */
    public ManagerController(ManagerService managerService){
        this.managerService = managerService;
    }

    /**
     * Log in the manager into the system
     * @param ctx the context of the log in
     */
    public void LogInManager(Context ctx) {
        String username = ctx.formParam("username");
        String password = ctx.formParam("password");
        if (managerService.logIn(username, password)) {
            ctx.render("/public/ManagerHome.html");
            currentUser = username;
            logData();
        }
        else {
            // do something here to say that it was a failed attempt
            ctx.render("/public/ManagerLogIn.html");
        }
    }

    /**
     * Check the log in status of an employee
     * @return The status of a logged in employee
     */
    public boolean CheckLogInStatus() {
        if (currentUser == null){
            return false;
        }
        return true;
    }

    /**
     * Gets the information of a manager to the screen
     * @param ctx
     */
    public void GetManagerInformation(Context ctx) {
        Manager currentManager = managerService.getDao().getManager(currentUser);
        ctx.json(currentManager);
    }

    /** Gets the current manager and returns it as a string
     * @return the current manager
     */
    public String getCurrentManager() {
        return currentUser;
    }

    /**
     * Logs out the current user
     */
    public void logOut() {
        currentUser = null;
    }

    /**
     * Logs the information on log ins
     */
    @Override
    public void logData() {
        rootLogger.info("Manager {} has successfully logged in.",currentUser);
    }
}
