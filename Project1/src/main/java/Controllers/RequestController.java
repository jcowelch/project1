package Controllers;

import POJOS.Request;
import Service.RequestService;
import Utility.Logging;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import io.javalin.http.Context;
import org.json.JSONArray;


/**
 * Controller for handling requests created by the user
 *
 * @author Jeremy
 */
public class RequestController implements Controller, Logging {

    private EmployeeController employeeController;
    private ManagerController managerController;
    private RequestService requestService;

    /**
     * Create request controller from a constructor
     * @param requestService The service for requests
     * @param employeeController The employee controller
     * @param managerController The manager controller
     */
    public RequestController(RequestService requestService, EmployeeController employeeController, ManagerController managerController) {
        this.requestService = requestService;
        this.employeeController = employeeController;
        this.managerController = managerController;
    }

    /**
     * Creates a request for a current user and stores it in the dao
     * @param ctx The context of the screen
     */
    public void CreateRequest(Context ctx) {
        String username = ctx.formParam("user");
        String value = ctx.formParam("value");
        String subject = ctx.formParam("subject");
        double cost;

        try {
            assert value != null;
            cost = Double.valueOf(value);
        } finally {

        }
        if (subject == null){
            ctx.redirect("/EmployeeHome");
        }

        Request req = new Request(username,subject,cost,'p',"N/A");

        requestService.CreateRequest(req);
        logDataRequest(username, value, subject);
        ctx.redirect("/EmployeeHome");
    }

    /**
     * Gets a specific request for a current user.
     * @param ctx The context of the screen
     */
    public void GetEmployeeRequest(Context ctx) {
        // get the user and the list of requests for the user
        String currentUser = employeeController.getCurrentUser();
        GetRequest(currentUser,ctx);
    }

    /**
     * Gets all requests and displays them to the screen
     * @param ctx The context of the screen
     */
    public void GetAllRequests(Context ctx) {
        MongoCollection<Request> requests = requestService.getDao().getRequests();

        // create an array and iterate through it, then return it to the screen to display
        JSONArray response = new JSONArray();
        for (Request request : requests.find()) {
            response.put(request);
        }
        ctx.json(response);
    }

    /**
     * Gets the request for a general user and displays it to the screen
     * @param user The current user
     * @param ctx the current context
     */
    private void GetRequest(String user, Context ctx){
        FindIterable<Request> requests = requestService.getDao().getUserRequests(user);

        // create an array and iterate through it, then return it to the screen to display
        JSONArray response = new JSONArray();
        for (Request request : requests) {
            response.put(request);
        }
        ctx.json(response);
    }

    public void updateRequest(Context ctx) {
        String name = ctx.formParam("username");
        String price = ctx.formParam("cost");
        String subject = ctx.formParam("subject");
        String status = ctx.formParam("status");
        String manager = managerController.getCurrentManager();

        double cost;
        try {
            cost = Double.valueOf(price);
        } finally {

        }

        if (status.equals("approve")){
            requestService.updateRequest(name,cost,subject,'a',manager);
        }
        else if (status.equals("deny")){
            requestService.updateRequest(name,cost,subject,'d',manager);
        }

        logDataUpdate(name, price, subject, status, manager);
        ctx.redirect("/Manager/Requests");
    }

    /**
     * Gets the requests for an employee
     * @param ctx The context of the page
     */
    public void getEmployeeRequest(Context ctx) {
        String employee = ctx.formParam("employee");
        FindIterable<Request> requests = requestService.getDao().getUserRequests(employee);
        String html = "<!DOCTYPE html>" +
                "<html lang=\"en\">" +
                "<head>" +
                "<link rel=\"stylesheet\" href=\"styles.css\">" +
                "<meta_charset=\"UTF-8\">" +
                "<title>View Employee</title>" +
                "</head>" +
                "<body>" +
                "<h2>Requests for " + requests.first().getCreator() + "</h2>" +
                "<a href=\"/ManagerHome\">Return to manager home</a><br>" +
                "<table style=\"width:100%\">" +
                "<tr>" +
                "<th>Subject</th>" +
                "<th>Value</th>" +
                "<th>Status</th>" +
                "<th>Manager</th>" +
                "</tr>";


        // create an array and iterate through it, then return it to the screen to display
        for (Request request : requests) {
            html += "<tr>" +
                    "<td>"+request.getSubject()+"</td>" +
                    "<td>"+request.getCost()+"</td>" +
                    "<td>"+request.getStatus()+"</td>" +
                    "<td>"+request.getManager()+"</td>" +
                    "</tr>";
        }

        html += "</table>" +
                "</body>" +
                "</html>";
        ctx.html(html);
    }

    /**
     * Log the user data
     */
    @Override
    public void logData() { }

    /**
     * Logs a creation of a request
     * @param username the creator of the request
     * @param value the value of the request
     * @param subject the subject of the request
     */
    private void logDataRequest(String username, String value, String subject) {
        rootLogger.info("Employee {} created a request of {} for {}",username, value, subject);
    }

    /**
     *
     * @param name
     * @param price
     * @param subject
     * @param status
     * @param manager
     */
    private void logDataUpdate(String name, String price, String subject, String status, String manager) {
    rootLogger.info("Updated request of {} {} {} {} to {}",name,price,subject,manager,status);
    }
}
