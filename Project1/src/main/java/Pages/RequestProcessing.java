package Pages;

import Controllers.DisplayController;
import Controllers.RequestController;
import io.javalin.Javalin;

import java.util.HashMap;

/**
 * Class for processing requests on a given page. Allows for connection of pages for requests
 *
 * @author Jeremy
 */
public class RequestProcessing implements Page{

    private Javalin javalin;
    private HashMap<String,Object> data;

    /**
     * Create the Request Processing page to allow the pages to work
     * @param javalinApp
     * @param database
     */
    public RequestProcessing(Javalin javalinApp, HashMap<String, Object> database) {
        javalin = javalinApp;
        data = database;
    }

    /**
     * Do the pages and their important tasks
     */
    @Override
    public void doPage() {
        javalin.get("/Employee/SubmitRequest",ctx-> {
            ((DisplayController) data.get("DisplayController")).displaySubmitRequest(ctx); });
        javalin.post("/Employee/SubmitRequest",ctx->{
            ((RequestController) data.get("RequestController")).CreateRequest(ctx); });

        javalin.get("/Manager/Requests",ctx->{
            ((DisplayController) data.get("DisplayController")).displayManagerRequests(ctx); });

        javalin.post("/Manager/Requests",ctx->{
            ((RequestController) data.get("RequestController")).updateRequest(ctx); });

    }
}
