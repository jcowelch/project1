package Pages;

import Controllers.EmployeeController;
import Controllers.ManagerController;
import Controllers.RequestController;
import io.javalin.Javalin;

import java.util.HashMap;

/**
 * The page for handling all data. Effectively "invisible"
 * @author Jeremy
 */
public class Data implements Page{
    private Javalin javalin;
    private HashMap<String,Object> data;

    /**
     * Creates the data object with the data and the javalin app
     * @param javalinApp
     * @param database
     */
    public Data(Javalin javalinApp, HashMap<String,Object> database) {
        javalin = javalinApp;
        data = database;
    }

    /**
     * Do the page and display all the information when its required
     */
    @Override
    public void doPage() {
        javalin.post("/UserData",ctx->{
            ((EmployeeController) data.get("EmployeeController")).GetEmployeeInformation(ctx); });

        javalin.post("/ManagerData",ctx->{
            ((ManagerController) data.get("ManagerController")).GetManagerInformation(ctx); });

        javalin.post("/RequestEmployeeData",ctx->{
            ((RequestController) data.get("RequestController")).GetEmployeeRequest(ctx); });

        javalin.post("/RequestManagerData", ctx-> {
            ((RequestController) data.get("RequestController")).GetAllRequests(ctx); });

        javalin.post("/EmployeeData",ctx->{
            ((EmployeeController) data.get("EmployeeController")).getEmployees(ctx); });

    }
}
