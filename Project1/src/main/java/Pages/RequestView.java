package Pages;

import Controllers.DisplayController;
import Controllers.EmployeeController;
import Controllers.ManagerController;
import Controllers.RequestController;
import io.javalin.Javalin;

import java.util.HashMap;

/**
 * Class for handling all request view methods, including employee and manager
 *
 * @author Jeremy
 */
public class RequestView implements Page{

    private Javalin javalin;
    private HashMap<String,Object> data;

    /**
     * Constructor for the request view
     * @param javalinApp Javalin app for the page
     * @param database database for the page
     */
    public RequestView(Javalin javalinApp, HashMap<String,Object> database) {
     javalin = javalinApp;
     data = database;
    }

    /**
     * Prepares the pages for use
     */
    @Override
    public void doPage() {
        javalin.get("/Employee/ViewRequests", ctx -> {
            ((DisplayController) data.get("DisplayController")).displayViewRequests(ctx); });

        javalin.get("/Manager/View", ctx -> {
            ((DisplayController) data.get("DisplayController")).displayManagerView(ctx); });

        javalin.post("/Manager/View", ctx -> {
            ctx.redirect("/Manager/View"); });

        javalin.post("/Manager/View/Employee", ctx -> {
            ((RequestController) data.get("RequestController")).getEmployeeRequest(ctx); });
    }
}
