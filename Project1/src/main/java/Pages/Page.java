package Pages;

/**
 * Abstract page which only contains the necessary information to handle a page
 *
 * @author Jeremy
 */
public interface Page {
    public void doPage();
}
