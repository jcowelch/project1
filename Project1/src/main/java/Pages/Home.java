package Pages;

import Controllers.DisplayController;
import io.javalin.Javalin;

import java.util.HashMap;

/**
 * Page for handling the employee home page.
 *
 * @author Jeremy
 */
public class Home implements Page{

    Javalin javalin;
    HashMap<String,Object> data;

    /**
     * Constructor for the employee home. Redirects to other pages
     * @param javalinApp The app being used on the page
     * @param database The database being used on the page
     */
    public Home(Javalin javalinApp, HashMap<String,Object> database) {
        javalin = javalinApp;
        data = database;
    }

    /**
     * Does the visuals for the page.
     */
    @Override
    public void doPage() {
        javalin.get("/", ctx->{
            ((DisplayController) (data.get("DisplayController"))).displayHomeScreen(ctx); });

        javalin.get("/EmployeeHome", ctx -> {
            ((DisplayController) (data.get("DisplayController"))).displayEmployeeHome(ctx); });

        javalin.get("/ManagerHome",ctx->{
            ((DisplayController) (data.get("DisplayController"))).displayManagerHome(ctx); });
    }
}
