package Pages;

import Controllers.DisplayController;
import Controllers.EmployeeController;
import io.javalin.Javalin;

import java.util.HashMap;

/**
 * Page for updating information for employees
 *
 * @author Jeremy
 */
public class UpdateInformation implements Page{
    private Javalin javalin;
    private HashMap<String,Object> data;

    /**
     * Constructor for the UpdateInformationPage. Allows information to be updated for users
     * @param javalinApp The javalin app required
     * @param database The database
     */
    public UpdateInformation(Javalin javalinApp, HashMap<String,Object> database) {
        javalin = javalinApp;
        data = database;
    }

    /**
     * Prepares the pages for use
     */
    @Override
    public void doPage() {
        javalin.get("/Employee/Update", ctx->{
            ((DisplayController) (data.get("DisplayController"))).displayUpdate(ctx); });
        javalin.post("/Employee/Update",ctx->{
            ((EmployeeController) data.get("EmployeeController")).ChangePassword(ctx); });
    }
}
