package Pages;

import Controllers.EmployeeController;
import Controllers.ManagerController;
import DAO.MongoDao;
import io.javalin.Javalin;

import java.util.HashMap;

/**
 * Page for handling all employee log in actions
 * @author Jeremy
 */
public class LogIn implements Page{
    private Javalin javalin;
    private HashMap<String, Object> data;

    /**
     * Constructor for the page. Takes the javalin app and the db
     * @param javalinApp The javalin app it is a part of
     * @param database The database used on this page
     */
    public LogIn(Javalin javalinApp, HashMap<String,Object> database) {
        javalin = javalinApp;
        data = database;
    }

    /**
     * Does the pages actions and displays information
     */
    @Override
    public void doPage() {
        javalin.get("/Employee", ctx -> {
            ctx.render("/public/EmployeeLogIn.html"); });
        javalin.post("/Employee", ctx->{
            ((EmployeeController) data.get("EmployeeController")).LogInEmployee(ctx); });

        javalin.get("/Manager",ctx->{
            ctx.render("/public/ManagerLogIn.html"); });
        javalin.post("/Manager",ctx->{
            ((ManagerController) data.get("ManagerController")).LogInManager(ctx); });
    }
}
