import io.javalin.Javalin;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Main {
    public static void main(String args[]) {
        BasicConfigurator.configure();

        // set up the parameters for the application
        Javalin javalinApp = Javalin.create(config -> {
            config.enableCorsForAllOrigins();
            config.addStaticFiles("/public");
        }).start(7777);


        // create the new application
        Application app = new Application(javalinApp, "Project1");

        // run the application once set up
        app.run();
    }
}
