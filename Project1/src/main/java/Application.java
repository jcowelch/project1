import Controllers.*;
import DAO.*;
import Pages.*;
import Service.*;

import Utility.AbstractApplication;
import io.javalin.Javalin;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.util.HashMap;

/**
 * Application for handling the web application
 *
 * @author Jeremy
 */
public class Application extends AbstractApplication
{

    private Javalin javalin;
    private HashMap<String, Object> data = new HashMap<>();

    /**
     * Constructor for the application. Allows the app to prepare itself
     * @param javalinApp The javalin application being used
     * @param dbName The db name
     */
    Application(Javalin javalinApp, String dbName){
        javalin = javalinApp;
        initialize(dbName);
    }

    /**
     * Initializes the app. Prepares the db
     * @param dbName The name of the database to be used to form the dao
     */
    @Override
    protected void initialize(String dbName) {
        initializeData(dbName);
        initializeLogger();
    }

    /**
     * Prepares the data and puts it together for program use
     * @param dbName The name of the database being used to form the data
     */
    private void initializeData(String dbName){
        MongoDao dao = new MongoDao(dbName);

        EmployeeService employeeService = new EmployeeService(dao);
        ManagerService managerService = new ManagerService(dao);
        RequestService requestService = new RequestService(dao);

        EmployeeController employeeController = new EmployeeController(employeeService);
        ManagerController managerController = new ManagerController(managerService);
        RequestController requestController = new RequestController(requestService,employeeController,managerController);
        DisplayController displayController = new DisplayController(employeeController, managerController);

        data.put("Dao",dao);

        data.put("EmployeeService",employeeService);
        data.put("ManagerService",managerService);
        data.put("RequestService", requestService);

        data.put("EmployeeController",employeeController);
        data.put("ManagerController",managerController);
        data.put("RequestController",requestController);
        data.put("DisplayController",displayController);
    }

    /**
     * Prepare the logger and clean it up so it doesn't display on the ide log
     */
    private static void initializeLogger() {
        Logger mongoLogger = Logger.getLogger("org.mongodb.driver");
        Logger javalinLogger = Logger.getLogger("io.javalin.Javalin");
        Logger eclipseLogger = Logger.getLogger("org.eclipse.jetty");
        Logger thymeleafLogger = Logger.getLogger("org.thymeleaf");


        mongoLogger.setLevel(Level.FATAL);
        javalinLogger.setLevel(Level.FATAL);
        eclipseLogger.setLevel(Level.FATAL);
        thymeleafLogger.setLevel(Level.FATAL);

    }

    /**
     * Runs the application
     */
    @Override
    public void run() {
        runPages();
    }

    /**
     * Runs and sets up the pages to allow for corrent links to display
     */
    private void runPages() {
        (new LogIn(javalin, data)).doPage();
        (new Home(javalin, data)).doPage();
        (new RequestProcessing(javalin, data)).doPage();
        (new Data(javalin,data)).doPage();
        (new RequestView(javalin,data)).doPage();
        (new UpdateInformation(javalin,data)).doPage();
    }

    /**
     * Quits the application
     */
    @Override
    public void quit() {
        javalin.stop();
    }

}