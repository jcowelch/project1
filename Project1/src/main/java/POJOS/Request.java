package POJOS;

import io.javalin.Javalin;

import java.util.HashMap;

/**
 * Pojo for requests
 *
 * @author Jeremy
 */
public class Request {
    private String creator;
    private String subject;
    private double cost;
    private char status;
    private String manager;

    /**
     * Default no args constructor
     */
    public Request() {}

    /**
     * Constructor with all slots for the request. Makes it work
     * @param Creator The creator of the request
     * @param Subject The subject of the request
     * @param Cost The value of the request
     * @param Status The status of the request
     * @param Manager The manager that approves the request
     */
    public Request(String Creator, String Subject, double Cost, char Status, String Manager)
    {
        creator = Creator;
        subject = Subject;
        cost = Cost;
        status = Status;
        manager = Manager;
    }

    /**
     * Converts the request to a string;
     * @return
     */
    @Override
    public String toString() {
        return "Request{" +
                "creator='" + creator + '\'' +
                ", subject='" + subject + '\'' +
                ", cost=" + cost +
                ", status='" + status + '\'' +
                ", manager='" + manager + '\'' +
                '}';
    }

    /**
     * Getter for creator
     * @return creator
     */
    public String getCreator() {
        return creator;
    }

    /**
     * Setter for creator
     * @param creator new creator
     */
    public void setCreator(String creator) {
        this.creator = creator;
    }

    /**
     * Getter for subject
     * @return subject
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Setter for subject
     * @param subject new subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * Getter for cost
     * @return cost
     */
    public double getCost() {
        return cost;
    }

    /**
     * Setter for cost
     * @param cost new cost
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * Getter for status
     * @return status
     */
    public char getStatus() {
        return status;
    }

    /**
     * Setter for status
     * @param status new status
     */
    public void setStatus(char status) {
        this.status = status;
    }

    /**
     * Getter for manager
     * @return manager
     */
    public String getManager() {
        return manager;
    }

    /**
     * Setter for manager
     * @param manager new manager
     */
    public void setManager(String manager) {
        this.manager = manager;
    }
}
