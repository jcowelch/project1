package POJOS;

/**
 * Pojo for a manager
 * @author Jeremy
 */
public class Manager extends User {
    private String username;
    private String password;

    /**
     * Default no args constructor
     */
    public Manager() {
    }

    /**
     * Args constructor for a manager pojo
     * @param username the username of the manager
     * @param password the password of the manager
     */
    public Manager(String username, String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Getter for username
     * @return username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Setter for username
     * @param username new username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Getter for password
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter for password
     * @param password new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Converts manager content to a string
     * @return manager information as a string
     */
    @Override
    public String toString() {
        return "Manager{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
