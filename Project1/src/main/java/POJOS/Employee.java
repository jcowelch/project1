package POJOS;

/**
 * Pojo for an employee.
 *
 * @author Jeremy
 */
public class Employee extends User {
    private String username;
    private String password;

    /**
     * default constructor
     */
    public Employee(){}

    /**
     * Argument constructor
     * @param user The username
     * @param pass The password
     */
    public Employee(String user, String pass) {
        username = user;
        password = pass;
    }

    /**
     * Gets the username
     * @return The username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username
     * @param username The username to set to
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the password
     * @return The password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password
     * @param password The new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Converts the employee data into a string
     * @return The string form of the employee
     */
    @Override
    public String toString() {
        return "Employee{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
