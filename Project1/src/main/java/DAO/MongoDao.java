package DAO;

import POJOS.Employee;
import POJOS.Manager;
import POJOS.Request;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;

import static com.mongodb.client.model.Filters.*;

import com.mongodb.client.model.Updates;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.conversions.Bson;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

/**
 * Dao for handling the list of information. Gets and holds all the information from the database
 *
 * @author Jeremy_Cowelchuk
 */
public class MongoDao implements DAO{
    MongoCollection<Employee> Employees;
    MongoCollection<Request> Requests;
    MongoCollection<Manager> Managers;

    /**
     * Constructor for the dao. Gets the information and stores it correctly
     * @param dbName The name of the DB
     */
    public MongoDao(String dbName){
        try {
            ConnectionString connectionString = new ConnectionString("mongodb://localhost:27017/"+dbName);
            CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
            CodecRegistry codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
            MongoClientSettings settings = MongoClientSettings.builder()
                    .applyConnectionString(connectionString)
                    .retryWrites(true)
                    .codecRegistry(codecRegistry)
                    .build();
            MongoClient client = MongoClients.create(settings);

            MongoDatabase database = client.getDatabase(dbName);
            this.Employees = database.getCollection("Employees",Employee.class);
            this.Requests = database.getCollection("Requests",Request.class);
            this.Managers = database.getCollection("Managers",Manager.class);

        } catch (Exception e) { e.printStackTrace();
        }
    }


    /**
     * Gets the MongoCollection of Employees
     * @return Gets the collection of employees
     */
    public MongoCollection<Employee> getEmployees() {
        return Employees;
    }

    /**
     * Returns an employee that matches the username
     * @param currentUser The current user
     * @return The employee object of the current user
     */
    public Employee getEmployee(String currentUser) {
        return Employees.find(eq("username",currentUser)).first();
    }

    /**
     * Updates the password of a given user
     * @param currentUser The current user
     * @param newPassword The new password
     */
    public void UpdateEmployeePassword(String currentUser, String newPassword){

        // compile it to be a new update
        List<Bson> updates = new ArrayList<>();
        updates.add(Updates.set("username",currentUser));
        updates.add(Updates.set("password",newPassword));

        Employees.findOneAndUpdate(eq("username",currentUser),updates);
    }

    /**
     * Gets the MongoCollection of Requests
     * @return Gets the collection of requests
     */
    public MongoCollection<Request> getRequests() {
        return Requests;
    }

    /**
     * Adds a request to the database
     * @param request The request being added to the db
     */
    public void addRequest(Request request) {
        Requests.insertOne(request);
    }

    /**
     * Gets a collection of requests for a particular user
     * @param currentUser The user to get for
     * @return The collection of requests
     */
    public FindIterable<Request> getUserRequests(String currentUser) {
        return Requests.find(eq("creator",currentUser));
    }

    /**
     * Get a list of all managers
     * @return the managers
     */
    public MongoCollection<Manager> getManagers() {
        return Managers;
    }

    /**
     * returns a specific manager
     * @param currentUser The manager to find
     * @return The manager
     */
    public Manager getManager(String currentUser) {
        return Managers.find(eq("username",currentUser)).first();
    }

    /**
     * Updates a request in the db
     * @param name the name of the employee
     * @param cost the cost of the request
     * @param subject the subject of the request
     * @param status the status of the request
     * @param manager the manager of the request
     */
    public void UpdateRequest(String name, double cost, String subject, char status, String manager) {
        List<Bson> updates = new ArrayList<>();
        updates.add(Updates.set("creator",name));
        updates.add(Updates.set("cost",cost));
        updates.add(Updates.set("subject",subject));
        updates.add(Updates.set("status",status));
        updates.add(Updates.set("manager",manager));

        Requests.findOneAndUpdate(and
                (eq("creator",name),
                eq("cost",cost),
                eq("subject",subject),
                eq("status","p")),
                updates);
    }
}
