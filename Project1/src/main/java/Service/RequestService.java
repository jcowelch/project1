package Service;

import DAO.MongoDao;
import POJOS.Request;

/**
 * Service for handling requests. Does the work 'below' user input
 * @author Jeremy
 */
public class RequestService {

    private MongoDao dao;

    /**
     * Constructor. Takes a dao
     * @param Dao The dao of the service
     */
    public RequestService(MongoDao Dao) {
        dao = Dao;
    }

    /**
     * Gets a dao from the service.
     * @return The dao
     */
    public MongoDao getDao() {
        return dao;
    }

    /**
     * Adds a service to the Dao. Does nothing else
     */
    public void CreateRequest(Request request){
        dao.addRequest(request);
    }

    /**
     * Updates a request in the dao
     * @param name The name of the request
     * @param cost The cost of the request
     * @param subject The subject of the request
     * @param status The status of the request
     * @param manager The manager of the request
     */
    public void updateRequest(String name, double cost, String subject, char status, String manager) {
        dao.UpdateRequest(name,cost,subject,status,manager);
    }
}
