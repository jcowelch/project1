package Service;

import Controllers.ManagerController;
import DAO.MongoDao;
import POJOS.Employee;
import POJOS.Manager;

/**
 * Service for handling the manager pojo
 * @author Jeremy
 */
public class ManagerService {
    private MongoDao dao;

    /**
     * Constructor for the manager service
     * @param dao The dao of the service
     */
    public ManagerService(MongoDao dao){
        this.dao = dao;
    }

    /**
     * Get the user information and confirm it exists
     * @param username the username of the user
     * @param password the password of the user
     * @return The status of the log in
     */
    public boolean logIn(String username, String password) {
        //iterate through all valid employees
        for (Manager manager : dao.getManagers().find()) {
            // check credentials against a user
            if (username.equals(manager.getUsername()) && password.equals(manager.getPassword())) {
                //log them in if its true
                return true;
            }
        }
        return false;
    }

    /**
     * Gets the dao of a service
     * @return the dao
     */
    public MongoDao getDao() {
        return dao;
    }
}
