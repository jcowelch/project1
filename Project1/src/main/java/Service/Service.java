package Service;

import DAO.MongoDao;

/**
 * Does all the "middlework" between the user input and the data. Input validation, new accounts, and account changes
 * occur in the service layer.
 * @author Jeremy
 */
public interface Service {
    MongoDao getDao();
}
