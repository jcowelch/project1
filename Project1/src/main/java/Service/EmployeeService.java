package Service;

import DAO.MongoDao;
import POJOS.Employee;
import com.mongodb.client.MongoCollection;

/**
 * Log in service for handling all log in attempts
 * @author Jeremy
 */
public class EmployeeService implements Service{
    private MongoDao dao;

    /**
     * Constructor for the service. Takes the Dao
     * @param Dao The dao being entered
     */
    public EmployeeService(MongoDao Dao){
        dao = Dao;
    }

    /**
     * Logs in an employee
     * @param username The username being entered
     * @param password The password being entered
     * @return The success of the log in attempt
     */
    public boolean employeeLogIn(String username, String password){
        //iterate through all valid employees
        for (Employee employee : dao.getEmployees().find()) {
            // check credentials against a user
            if (username.equals(employee.getUsername()) && password.equals(employee.getPassword())) {
                //log them in if its true
                return true;
            }
        }
        return false;
    }

    /**
     * Get the dao to use with employee controller
     * @return
     */
    @Override
    public MongoDao getDao() {
        return dao;
    }

    /**
     * Gets the collection of all employees
     * @return The employees
     */
    public MongoCollection<Employee> getEmployees() {
        return getDao().getEmployees();
    }
}
