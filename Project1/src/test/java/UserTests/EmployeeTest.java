package UserTests;

import Controllers.DisplayController;
import Controllers.EmployeeController;
import Controllers.ManagerController;
import Controllers.RequestController;
import DAO.MongoDao;
import POJOS.Employee;
import Pages.LogIn;
import Service.*;
import io.javalin.Javalin;
import io.javalin.http.Context;
import org.junit.*;

import java.util.HashMap;

import static org.mockito.Mockito.*;

public class EmployeeTest {

    static Javalin javalin;
    static HashMap<String,Object> data = new HashMap<>();
    private Context ctx = mock(Context.class);

    @BeforeClass
    public static void Start(){
        javalin = Javalin.create(config -> {
        }).start(6002);

        MongoDao dao = new MongoDao("Project1Testing");
        EmployeeService employeeService = new EmployeeService(dao);
        ManagerService managerService = new ManagerService(dao);
        RequestService requestService = new RequestService(dao);

        EmployeeController employeeController = new EmployeeController(employeeService);
        ManagerController managerController = new ManagerController(managerService);
        RequestController requestController = new RequestController(requestService,employeeController,managerController);
        DisplayController displayController = new DisplayController(employeeController, managerController);

        data.put("Dao",dao);

        data.put("EmployeeService",employeeService);
        data.put("ManagerService",managerService);
        data.put("RequestService", requestService);

        data.put("EmployeeController",employeeController);
        data.put("ManagerController",managerController);
        data.put("RequestController",requestController);
        data.put("DisplayController",displayController);
    }

    @AfterClass
    public static void End(){
        javalin.stop();
    }

    @Test
    public void EmployeeToString() {
        Employee employee = new Employee("username","password");
        employee.toString();
    }

    @Test
    public void LogInSuccess() {
        when( ctx.formParam("username") ).thenReturn("jimmy");
        when( ctx.formParam("password") ).thenReturn("smith");

        ((EmployeeController) data.get("EmployeeController")).LogInEmployee(ctx);
    }

    @Test
    public void LogInFailure(){
        when( ctx.formParam("username") ).thenReturn("wrong");
        when( ctx.formParam("password") ).thenReturn("creds");

        ((EmployeeController) data.get("EmployeeController")).LogInEmployee(ctx);
    }

    @Test
    public void GetEmployeeInformation(){
        //log in the user to set "currentUser"
        when( ctx.formParam("username") ).thenReturn("jimmy");
        when( ctx.formParam("password") ).thenReturn("smith");
        ((EmployeeController) data.get("EmployeeController")).LogInEmployee(ctx);

        // check to see their information
        ((EmployeeController) data.get("EmployeeController")).GetEmployeeInformation(ctx);
    }

    @Test
    public void ChangeEmployeePassword(){
        //log in the user to set "currentUser"
        when( ctx.formParam("username") ).thenReturn("jimmy");
        when( ctx.formParam("password") ).thenReturn("smith");
        ((EmployeeController) data.get("EmployeeController")).LogInEmployee(ctx);

        // change their password to "password"
        when (ctx.formParam("updatePassword")).thenReturn("smith");
        ((EmployeeController) data.get("EmployeeController")).ChangePassword(ctx);
    }

    @Test
    public void GetEmployees(){
        ((EmployeeController) data.get("EmployeeController")).getEmployees(ctx);
    }
}
