package UserTests;

import Controllers.DisplayController;
import Controllers.EmployeeController;
import Controllers.ManagerController;
import Controllers.RequestController;
import DAO.MongoDao;
import POJOS.Employee;
import POJOS.Manager;
import Service.EmployeeService;
import Service.ManagerService;
import Service.RequestService;
import io.javalin.Javalin;
import io.javalin.http.Context;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ManagerTests {

    private static HashMap<String,Object> data = new HashMap<>();
    private static Javalin javalin;
    private Context ctx = mock(Context.class);


    @BeforeClass
    public static void start(){
        javalin = Javalin.create(config -> {
        }).start(6003);

        MongoDao dao = new MongoDao("Project1Testing");
        EmployeeService employeeService = new EmployeeService(dao);
        ManagerService managerService = new ManagerService(dao);
        RequestService requestService = new RequestService(dao);

        EmployeeController employeeController = new EmployeeController(employeeService);
        ManagerController managerController = new ManagerController(managerService);
        RequestController requestController = new RequestController(requestService,employeeController,managerController);
        DisplayController displayController = new DisplayController(employeeController, managerController);

        data.put("Dao",dao);

        data.put("EmployeeService",employeeService);
        data.put("ManagerService",managerService);
        data.put("RequestService", requestService);

        data.put("EmployeeController",employeeController);
        data.put("ManagerController",managerController);
        data.put("RequestController",requestController);
        data.put("DisplayController",displayController);
    }

    @AfterClass
    public static void End(){
        javalin.stop();
    }

    @Test
    public void toStringTest(){
        Manager manager = new Manager("username","password");
        manager.toString();
    }

    @Test
    public void LogInSuccess() {
        when( ctx.formParam("username") ).thenReturn("john.smith");
        when( ctx.formParam("password") ).thenReturn("password");

        ((ManagerController) data.get("ManagerController")).LogInManager(ctx);
    }

    @Test
    public void LogInFailure(){
        when( ctx.formParam("username") ).thenReturn("wrong");
        when( ctx.formParam("password") ).thenReturn("creds");

        ((ManagerController) data.get("ManagerController")).LogInManager(ctx);
    }

    @Test
    public void GetManagerInformation(){
        ((ManagerController) data.get("ManagerController")).GetManagerInformation(ctx);
    }

    @Test
    public void GetManager(){
        ((ManagerController) data.get("ManagerController")).getCurrentManager();
    }

}
