import Pages.*;
import io.javalin.Javalin;
import org.junit.*;

import java.util.HashMap;

public class PageTests {
    HashMap<String,Object> data = new HashMap<>();
    static Javalin javalin;

    @BeforeClass
    public static void BeforePage() {
        javalin = Javalin.create(config -> {
        }).start(6005);
    }

    @AfterClass
    public static void Close(){
        javalin.stop();
    }

    @Test
    public void EmployeeLogInPage(){
        (new LogIn(javalin, data)).doPage();
    }

    @Test
    public void EmployeeHomePage() { (new Home(javalin, data)).doPage();}

    @Test
    public void DataPage() {(new Data(javalin,data)).doPage();}

    @Test
    public void RequestProcessingPage() {(new RequestProcessing(javalin,data)).doPage();}

    @Test
    public void RequestViewPage() {(new RequestView(javalin,data)).doPage();}

    @Test
    public void UpdateInformationPage() {(new UpdateInformation(javalin,data)).doPage();}
}
