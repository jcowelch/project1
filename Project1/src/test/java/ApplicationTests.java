import io.javalin.Javalin;
import org.junit.*;

public class ApplicationTests {

    static Javalin javalin;
    static Application app;

    @BeforeClass
    public static void Start(){
        javalin = Javalin.create(config -> {
        }).start(6004);
        app = new Application(javalin,"Project1Testing");
    }

    @Test
    public void Run(){
        app.run();
    }

    @Test
    public void Stop()
    {
        app.quit();
    }
}
