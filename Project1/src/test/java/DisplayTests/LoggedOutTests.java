package DisplayTests;

import Controllers.DisplayController;
import Controllers.EmployeeController;
import Controllers.ManagerController;
import Controllers.RequestController;
import DAO.MongoDao;
import Service.EmployeeService;
import Service.ManagerService;
import Service.RequestService;
import io.javalin.Javalin;
import io.javalin.http.Context;
import org.junit.*;

import java.util.HashMap;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LoggedOutTests {

    static Javalin javalin;
    static HashMap<String, Object> data = new HashMap<>();
    private Context ctx = mock(Context.class);

    @BeforeClass
    public static void BeforePage() {
        javalin = Javalin.create(config -> {
        }).start(6001);

        MongoDao dao = new MongoDao("Project1Testing");
        EmployeeService employeeService = new EmployeeService(dao);
        ManagerService managerService = new ManagerService(dao);
        RequestService requestService = new RequestService(dao);

        EmployeeController employeeController = new EmployeeController(employeeService);
        ManagerController managerController = new ManagerController(managerService);
        RequestController requestController = new RequestController(requestService,employeeController,managerController);
        DisplayController displayController = new DisplayController(employeeController, managerController);

        data.put("Dao", dao);

        data.put("EmployeeService", employeeService);
        data.put("ManagerService", managerService);
        data.put("RequestService", requestService);

        data.put("EmployeeController", employeeController);
        data.put("ManagerController", managerController);
        data.put("RequestController", requestController);
        data.put("DisplayController", displayController);
    }

    @AfterClass
    public static void End(){
        javalin.stop();
    }

    @Test
    public void EmployeeHome() {
        //check page
        ((DisplayController) data.get("DisplayController")).displayEmployeeHome(ctx);
    }

    @Test
    public void SubmitRequest() {
        //check page
        ((DisplayController) data.get("DisplayController")).displaySubmitRequest(ctx);
    }

    @Test
    public void ViewRequest() {
        //check page
        ((DisplayController) data.get("DisplayController")).displayViewRequests(ctx);
    }

    @Test
    public void Update() {
        //check page
        ((DisplayController) data.get("DisplayController")).displayUpdate(ctx);
    }

    @Test
    public void ManagerHome(){
        ((DisplayController) data.get("DisplayController")).displayManagerHome(ctx);
    }

    @Test
    public void ManagerRequests() { ((DisplayController) data.get("DisplayController")).displayManagerRequests(ctx);}

    @Test
    public void HomeScreen() { ((DisplayController) data.get("DisplayController")).displayHomeScreen(ctx);}

    @Test
    public void ManagerView(){
        ((DisplayController) data.get("DisplayController")).displayManagerView(ctx);
    }
}


