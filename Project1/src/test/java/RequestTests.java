import Controllers.DisplayController;
import Controllers.EmployeeController;
import Controllers.ManagerController;
import Controllers.RequestController;
import DAO.MongoDao;
import POJOS.Employee;
import POJOS.Request;
import Service.EmployeeService;
import Service.ManagerService;
import Service.RequestService;
import io.javalin.Javalin;
import io.javalin.http.Context;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;

import static org.mockito.Mockito.*;

public class RequestTests {

    static Javalin javalin;
    static HashMap<String,Object> data = new HashMap<>();
    private Context ctx = mock(Context.class);

    @BeforeClass
    public static void Start(){
        javalin = Javalin.create(config -> {
        }).start(6006);

        MongoDao dao = new MongoDao("Project1Testing");

        EmployeeService employeeService = new EmployeeService(dao);
        ManagerService managerService = new ManagerService(dao);
        RequestService requestService = new RequestService(dao);

        EmployeeController employeeController = new EmployeeController(employeeService);
        ManagerController managerController = new ManagerController(managerService);
        RequestController requestController = new RequestController(requestService,employeeController,managerController);
        DisplayController displayController = new DisplayController(employeeController, managerController);

        data.put("Dao",dao);

        data.put("EmployeeService",employeeService);
        data.put("ManagerService",managerService);
        data.put("RequestService", requestService);

        data.put("EmployeeController",employeeController);
        data.put("ManagerController",managerController);
        data.put("RequestController",requestController);
        data.put("DisplayController",displayController);
    }

    @AfterClass
    public static void End(){
        javalin.stop();
    }

    @Test
    public void RequestToString() {
        Request request = new Request("jimmy","car", 100, 'p', "johnny");
        request.toString();
    }
    @Test
    public void CreateRequest(){
        when(ctx.formParam("user")).thenReturn("jimmy");
        when(ctx.formParam("value")).thenReturn("100");
        when(ctx.formParam("subject")).thenReturn("supplies");

        ((RequestController) data.get("RequestController")).CreateRequest(ctx);
    }

    @Test (expected = AssertionError.class)
    public void CreateNullValue(){
        when(ctx.formParam("user")).thenReturn("jimmy");
        when(ctx.formParam("value")).thenReturn(null);
        when(ctx.formParam("subject")).thenReturn("supplies");

        ((RequestController) data.get("RequestController")).CreateRequest(ctx);
    }

    @Test
    public void CreateNullSubject(){
        when(ctx.formParam("user")).thenReturn("jimmy");
        when(ctx.formParam("value")).thenReturn("100");
        when(ctx.formParam("subject")).thenReturn(null);

        ((RequestController) data.get("RequestController")).CreateRequest(ctx);
    }

    @Test
    public void GetRequest() {
        //log in with 'jimmy' to make sure there's requests
        when( ctx.formParam("username") ).thenReturn("jimmy");
        when( ctx.formParam("password") ).thenReturn("smith");

        ((EmployeeController) data.get("EmployeeController")).LogInEmployee(ctx);

        //get the requests for jimmy
        ((RequestController) data.get("RequestController")).GetEmployeeRequest(ctx);
    }

    @Test
    public void GetAllRequests() {
        ((RequestController) data.get("RequestController")).GetAllRequests(ctx);
    }

    @Test
    public void ApproveRequest() {
        when (ctx.formParam("name")).thenReturn("jimmy");
        when (ctx.formParam("cost")).thenReturn("100");
        when (ctx.formParam("subject")).thenReturn("supplies");
        when (ctx.formParam("status")).thenReturn("approve");

        ((RequestController) data.get("RequestController")).updateRequest(ctx);
    }

    @Test
    public void DenyRequest() {
        when (ctx.formParam("name")).thenReturn("jimmy");
        when (ctx.formParam("cost")).thenReturn("100");
        when (ctx.formParam("subject")).thenReturn("supplies");
        when (ctx.formParam("status")).thenReturn("deny");

        ((RequestController) data.get("RequestController")).updateRequest(ctx);
    }

    @Test
    public void GetEmployeeRequests(){
        when (ctx.formParam("employee")).thenReturn("jimmy");
        ((RequestController) data.get("RequestController")).getEmployeeRequest(ctx);
    }
}
