function loadDoc() {
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

                let response = JSON.parse(this.responseText);
                let table = document.querySelector("table");
                let data = Object.keys(response[0]);
                generateTableHead(table, data);
                generateTable(table, response);
            }
      };
      xhttp.open("POST", "/RequestManagerData", true);
      xhttp.send();
}
var currentUser;
function loadUser() {
      var xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {

                let response = JSON.parse(this.responseText);
                document.getElementById("currentUser").innerHTML =
                response.username;
            }
      };
      xhttp.open("POST", "/ManagerData", true);
      xhttp.send();
}

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}

function generateTable(table, data) {
  let total = 0;
  for (let element of data) {
    let row = table.insertRow();
    for (key in element) {
          let cell = row.insertCell();
          let text = document.createTextNode(element[key]);
          if (element.status === 'p'){
               row.setAttribute("name","P");
          }
         else if (element.status === 'a'){
               row.setAttribute("name","A");
         }
         else{
                row.setAttribute("name","D");
         }
      cell.appendChild(text);
    }

    if (element.status === 'p'){
        //add button for manager approval
        let approveCell = row.insertCell();
        let approveButton = document.createElement("BUTTON");
        approveButton.innerHTML = "Approve";
        approveButton.setAttribute("id","approveButton"+total)
        approveButton.setAttribute("onclick","Approve(id)");
        approveCell.appendChild(approveButton);


        //add button for manager denial
        let denyCell = row.insertCell();
        let denyButton = document.createElement("BUTTON");
        denyButton.innerHTML = "Deny";
        denyButton.setAttribute("id","denyButton"+total);
        denyButton.setAttribute("onclick","Deny(id)");
        denyCell.appendChild(denyButton);
    }
    row.setAttribute("id","row"+total);
    total += 1;
  }
}

function Approve(id){
    let index = id.replace("approveButton","");
    let row = document.getElementById("row"+index);

    let form = document.createElement("FORM");
    form.setAttribute("method","post");
    form.setAttribute("action","/Manager/Requests");

    var username = document.createElement("input");
    username.setAttribute("type","text");
    username.setAttribute("name","username");

    var cost = document.createElement("input");
    cost.setAttribute("type","text");
    cost.setAttribute("name","cost");

    var subject = document.createElement("input");
    subject.setAttribute("type","text");
    subject.setAttribute("name","subject");

    var status = document.createElement("input");
    status.setAttribute("type","text");
    status.setAttribute("name","status");

    username.setAttribute("value",row.cells[0].innerHTML);
    cost.setAttribute("value",row.cells[2].innerHTML);
    subject.setAttribute("value",row.cells[1].innerHTML);
    status.setAttribute("value","approve");

    let button = document.getElementById(id);

    form.appendChild(username);
    form.appendChild(cost);
    form.appendChild(subject);
    form.appendChild(status);
    form.appendChild(button);

    document.body.appendChild(form);
}

function Deny(id){
    let index = id.replace("denyButton","");
    let row = document.getElementById("row"+index);

    let form = document.createElement("FORM");
    form.setAttribute("method","post");
    form.setAttribute("action","/Manager/Requests");

    var username = document.createElement("input");
    username.setAttribute("type","text");
    username.setAttribute("name","username");

    var cost = document.createElement("input");
    cost.setAttribute("type","text");
    cost.setAttribute("name","cost");

    var subject = document.createElement("input");
    subject.setAttribute("type","text");
    subject.setAttribute("name","subject");

    var status = document.createElement("input");
    status.setAttribute("type","text");
    status.setAttribute("name","status");

    username.setAttribute("value",row.cells[0].innerHTML);
    cost.setAttribute("value",row.cells[2].innerHTML);
    subject.setAttribute("value",row.cells[1].innerHTML);
    status.setAttribute("value","deny");

    let button = document.getElementById(id);

    form.appendChild(username);
    form.appendChild(cost);
    form.appendChild(subject);
    form.appendChild(status);
    form.appendChild(button);

    document.body.appendChild(form);
}



function Pending(){
    let p = document.getElementsByName('P');
    for (var index = 0;index<p.length;index++){
        p[index].style.display = "table-row";
    }
    let r = document.getElementsByName('R');
    for (var index = 0;index<p.length;index++){
        r[index].style.display = "none";
    }
}

function Approved(){
    let p = document.getElementsByName('P');
    for (var index = 0;index<p.length;index++){
        p[index].style.display = "none";
    }
    let r = document.getElementsByName('R');
    for (var index = 0;index<p.length;index++){
        r[index].style.display = "table-row";
    }
}

function Both(){
    let p = document.getElementsByName('P');
    for (var index = 0;index<p.length;index++){
        p[index].style.display = "table-row";
    }
    let r = document.getElementsByName('R');
    for (var index = 0;index<p.length;index++){
        r[index].style.display = "table-row";
    }
}

loadUser();

loadDoc();
Pending();