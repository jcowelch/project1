Employee Reimbursement System:

Description
===========
The enclosed project is a full-stack java web application for handling and creating employee reimbursement requests.
The project was designed using Java, Javalin, and MongoDB for the backend. The front end was designed using Javascript, HTML, CSS.
The project was tested using JUnit and has 92% coverage.

Technologies
============
Java 1.8
Javalin
JUnit
MongoDB
Javascript

Features
========
Employees are able to log in
Employees are able to create reimbursement requests
Employees are able to view their own requests
Employees are able to sort their own requests by status
Employees can change their password

Managers can log in
Managers can view all requests
Managers can approve or deny pending requests
Managers can view all Employees
Managers can view all requests by a single employee

Getting Started
===============
In order to run, run the main file, then connect to "localhost:7777/"

Usage
=====

Contributors
============
Jeremy Cowelchuk

License
=======

